// +build windows

package runtime

// RuntimeMetrics runtime监控 每隔一分钟定时上报runtime详细信息
func RuntimeMetrics() {
	getProfData()
	getMemStats()
}
