// openbsd的"syscall.Statfs_t"结构与其他unix不一致，这里单独处理
// +build openbsd

package runtime

import (
	"syscall"

	"trpc.group/trpc-go/trpc-go/metrics"
)

// getDiskUsage 获取磁盘空间指标
func getDiskUsage(path string) {
	fs := syscall.Statfs_t{}
	err := syscall.Statfs(path, &fs)
	if err != nil {
		return
	}
	diskAll := float64(fs.F_blocks * uint64(fs.F_bsize))
	diskFree := float64(fs.F_bfree * uint64(fs.F_bsize))
	diskUsed := diskAll - diskFree
	diskUsedFraction := diskUsed / diskAll

	metrics.Gauge("trpc.DiskFree(GB)").Set(diskFree / float64(GB))
	metrics.Gauge("trpc.DiskUsed(GB)").Set(diskUsed / float64(GB))
	metrics.Gauge("trpc.DiskUsedFraction(%)").Set(diskUsedFraction)
}
