// +build !windows,!openbsd

package runtime

import (
	"syscall"

	"trpc.group/trpc-go/trpc-go/metrics"
)

// getDiskUsage 获取磁盘空间指标
func getDiskUsage(path string) {
	fs := syscall.Statfs_t{}
	err := syscall.Statfs(path, &fs)
	if err != nil {
		return
	}
	diskAll := float64(fs.Blocks * uint64(fs.Bsize))
	diskFree := float64(fs.Bfree * uint64(fs.Bsize))
	diskUsed := diskAll - diskFree
	diskUsedFraction := diskUsed / diskAll

	metrics.Gauge("trpc.DiskFree(GB)").Set(diskFree / float64(GB))
	metrics.Gauge("trpc.DiskUsed(GB)").Set(diskUsed / float64(GB))
	metrics.Gauge("trpc.DiskUsedFraction(%)").Set(diskUsedFraction)
}
