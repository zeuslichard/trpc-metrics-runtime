package runtime

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"trpc.group/trpc-go/trpc-go/metrics"
	"trpc.group/trpc-go/trpc-go/plugin"

	trpc "trpc.group/trpc-go/trpc-go"
)

// InStatDomainName 内网统计域名
var InStatDomainName = "stat.trpc.oa.com"

// OutStatDomainName 外网统计域名
var OutStatDomainName = "stat.trpc.oa.com"

// ReportInterval 上报间隔
var ReportInterval = time.Hour * 24

func init() {
	// 启动统计数据监控，每隔24小时上报一次
	go StatReport(trpc.Version(), "")
}

// StatReport 每隔24小时上报统计数据
func StatReport(version string, plug string) {

	time.Sleep(time.Second * 3)
	plugin.WaitForDone(time.Minute) // 等待框架启动完成

	for {
		err := Stat(InStatDomainName, version, plug)
		if err != nil {
			metrics.Counter("InStatReportFail").Incr()
			err = Stat(OutStatDomainName, version, plug)
			if err != nil {
				metrics.Counter("OutStatReportFail").Incr()
			}
		}

		time.Sleep(ReportInterval)
	}
}

type statInfo struct {
	App       string `json:"app"`
	Server    string `json:"server"`
	IP        string `json:"ip"`
	Container string `json:"container"`
	Lang      string `json:"lang"`
	Version   string `json:"version"`
	Plugin    string `json:"plugin"`
}

// Stat 数据上报接口
func Stat(domain string, version string, plugin string) error {

	// http://stat.trpc.oa.com/report?app=xx&server=xx&ip=10.100.1.2&version=v0.1.0-rc.1&lang=go&container=xx
	stat := statInfo{
		App:       trpc.GlobalConfig().Server.App,
		Server:    trpc.GlobalConfig().Server.Server,
		IP:        trpc.GlobalConfig().Global.LocalIP,
		Container: trpc.GlobalConfig().Global.ContainerName,
		Lang:      "go",
		Version:   version,
		Plugin:    plugin,
	}
	statBytes, _ := json.Marshal(stat)
	rsp, err := http.Post(fmt.Sprintf("http://%s/api/trpc/stat/add", domain),
		"application/json", bytes.NewReader(statBytes))
	if err != nil {
		return err
	}
	rsp.Body.Close()

	return nil
}
